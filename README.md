# mongodb-api

A generic and hackable REST API that uses MongoDB.

## Overview

This is a super flexible REST API that works with MongoDB.

There are just a few prerequisites you need to setup in MongoDB in order to it to work.

## Prerequisites

All you need is a MongoDB database and two collections:
  - `user`: used for authentication. Each document in this collection must have at least these fields:
    + `_id`: the unique id that acts as a primary key obviously
    + `email`: this must be also unique
    + `passcode`: sha1 hash of the user's password
    + `enabled`: boolean that tells whether the user's enabled or not
  - `perm`: used for authorization. Each document here defines the set of permissions a user has. Must have fields in this collection are:
    + `user_id`: the user id acting as a foreign key
    + `perms`: an object of permissions, where in each property the key is the permission name and the value is an array of permitted operations.

    For example, if user `'123'` is allowed to write (create, update, delete) to the collection `'product'`, i.e. do a POST request to `/api/product` or a PUT/PATCH/DELETE request to `/api/product/:id` (where :id is the _id of a product), the operation would be allowed if this document is found in `perm`:

      ```js
      {
        user_id: '123',
        perms: {
          'product': ['write']
        }
      }
      ```

    Another example, if user `'123'` is allowed to create and update products of only a specific brand `'45'`, the operation would be allowed if `perms` for that user contains `'brand/45/product': ['create', 'update']` or `'product': ['create', 'update']`:

      ```js
      {
        user_id: '123',
        perms: {
          'brand/45/product': ['create', 'update']
        }
      }
      ```
    The configuration above allows authenticated user `'123'` to create documents in `product` collection via a POST request to `/api/product` if and only if the document has `brand_id: '45'`.

    Also note that user is not allowed to delete a product of brand '45' nor any product since they don't have neither 'delete' or 'write' permission for that.


## Making changes to this project

If you've cloned this project and want to amend the source code, here's
some information on what you should do:

### Install the dependencies:

```
npm install
```

### Run the project

To run in development mode on default port 8010 either:
 - run `npm run start:dev`; or
 - run `npm start`. Here you could also use a different port via environment variable `PORT`.

To run in production either:
 - set environment variable `PORT=80` and run `npm start`; or
 - run `npm run start:prod`.

### Run tests

Make sure you have mongodb set up. If not, follow these instructions.

1) From the command line start the mongodb client. Run `mongo`.
2) Switch to admin BD. Run `use admin` in mongodb client.
3) Login as admin user. Run `db.auth('admin', 'password')`.
4) Switch to test BD, the same configured for test in package.json by environment variable `DBURL`.
   Run `use api-test` in mongodb client.
5) Create the user to run the tests.
    ```js
    db.createUser({
      user: 'api-test',
      pwd: 'no-s3cret',
      roles: [
        { role: 'readWrite', db: 'api-test' }
      ]
    })
    ```

Now you can quit the mongodb client if you want: `quit()` and finally run: `npm test`
