const crypto = require('crypto');
const R85 = require('r85');

const r85 = new R85(process.env.R85KEY);
const encodeAuthToken = token => r85.encodeToString(JSON.stringify(token));
const decodeAuthToken = token => JSON.parse(r85.decodeToString(token));
const createAuthToken = async ({ crud, authHeader }) => {
  const [authType, encodedCredentials] = authHeader.split(' ');

  switch (authType) {
  case 'Basic': {
    const credentials = Buffer.from(encodedCredentials, 'base64').toString();
    const [email, password] = credentials.split(':');
    const passcode = crypto.createHash('sha1').update(password).digest('hex');
    const [user] = (await crud.user.read({ email, passcode, enabled: true })) || [];

    if (user) {
      const id = user._id.toString();
      const [{ perms = {} } = {}] = (await crud.perm.read({ user_id: user._id })) || [];
      const expiry = Date.now() + 600000;

      return { user_id: id, perms, expiry };
    }
    return null;
  }
  case 'Bearer': {
    const authToken = decodeAuthToken(encodedCredentials);

    if (authToken.expiry >= Date.now()) {
      return Object.assign(authToken, { expiry: Date.now() + 600000 });
    }
    return null;
  }
  default:
    return null;
  }
};

const authenticate = async ({
  ctx,
  crud,
  realm = 'This Website',
  createToken = createAuthToken,
}) => {
  try {
    const authHeader = ctx.request.get('Authorization') || '';
    const authToken = await createToken({ crud, authHeader });

    if (authToken) {
      ctx.response.set('X-Auth-Token', encodeAuthToken(authToken));
      return authToken;
    }
  } catch (err) {
    // empty
  }

  ctx.header['WWW-Authenticate'] = `Basic realm="${realm}", charset="UTF-8"`;
  ctx.throw(401, 'Unauthorized');
};

module.exports = { authenticate, createAuthToken, decodeAuthToken, encodeAuthToken, r85 };
