const { describe, it } = require('mocha');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const R85 = require('r85');
const { authenticate, createAuthToken, decodeAuthToken, encodeAuthToken } = require('./auth');

chai.use(sinonChai);

describe('api/crud/auth', () => {
  const { assert, expect } = chai;
  const { stub } = sinon;
  const r85 = new R85(process.env.R85KEY);

  describe('encodeAuthToken', () => {
    it('encodes an authentication token', () => {
      const token = {
        user_id: 1234,
        perm: {
          'course': ['read'],
          'course/2345': ['read', 'write'],
          'topic/12345/course': ['read', 'write'],
        },
      };
      const expected = r85.encodeToString(JSON.stringify(token));
      const actual = encodeAuthToken(token);

      expect(actual).to.equal(expected);
    });
  });

  describe('decodeAuthToken', () => {
    it('decodes an authentication token', () => {
      const expected = {
        user_id: 23456,
        perm: {
          'course/2345': ['read', 'write'],
          'topic/12345/course': ['read', 'write'],
        },
      };
      const token = r85.encodeToString(JSON.stringify(expected));
      const actual = decodeAuthToken(token);

      expect(actual).to.deep.equal(expected);
    });
  });

  describe('createAuthToken', () => {
    it('creates an authentication token', async () => {
      const crud = {
        user: {
          read: stub().resolves([{ _id: 1234 }]),
        },
        perm: {
          read: stub().resolves([{
            user_id: '1234',
            perms: {
              'course/2345': ['read', 'write'],
              'topic/12345/course': ['read', 'write'],
            },
          }]),
        },
      };
      const now = Date.now();
      const authHeader = 'Basic am9obkBleGFtcGxlLmNvbTpzZWNyZXQ=';
      const authToken = await createAuthToken({ crud, authHeader });

      expect(crud.user.read).to.have.been.calledOnceWith({
        email: 'john@example.com',
        passcode: 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4',
        enabled: true,
      });
      expect(crud.perm.read).to.have.been.calledOnceWith({ user_id: 1234 });
      expect(authToken.expiry).to.gte(now + 600000);
      expect(authToken).to.deep.equal({
        user_id: '1234',
        perms: {
          'course/2345': ['read', 'write'],
          'topic/12345/course': ['read', 'write'],
        },
        expiry: authToken.expiry,
      });
    });

    it('creates an authentication token when user has no configured permission', async () => {
      const crud = {
        user: {
          read: stub().resolves([{ _id: 1234 }]),
        },
        perm: {
          read: stub().resolves(),
        },
      };
      const now = Date.now();
      const authHeader = 'Basic am9obkBleGFtcGxlLmNvbTpzZWNyZXQ=';
      const authToken = await createAuthToken({ crud, authHeader });

      expect(crud.user.read).to.have.been.calledOnceWith({
        email: 'john@example.com',
        passcode: 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4',
        enabled: true,
      });
      expect(crud.perm.read).to.have.been.calledOnceWith({ user_id: 1234 });
      expect(authToken.expiry).to.gte(now + 600000);
      expect(authToken).to.deep.equal({
        user_id: '1234',
        perms: {},
        expiry: authToken.expiry,
      });
    });

    it('does not create an authentication token when user does not exist', async () => {
      const crud = {
        user: {
          read: stub().resolves(),
        },
        perm: {
          read: stub().resolves(),
        },
      };
      const authHeader = 'Basic am9obkBleGFtcGxlLmNvbTpzZWNyZXQ=';
      const authToken = await createAuthToken({ crud, authHeader });

      expect(crud.user.read).to.have.been.calledOnceWith({
        email: 'john@example.com',
        passcode: 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4',
        enabled: true,
      });
      expect(crud.perm.read).to.not.have.been.called;
      expect(authToken).to.be.null;
    });

    it('does not create an authentication token when authorization type is invalid', async () => {
      const crud = {
        user: {
          read: stub().resolves(),
        },
        perm: {
          read: stub().resolves(),
        },
      };
      const authHeader = 'Unknown am9obkBleGFtcGxlLmNvbTpzZWNyZXQ=';
      const authToken = await createAuthToken({ crud, authHeader });

      expect(crud.user.read).to.not.have.been.called;
      expect(crud.perm.read).to.not.have.been.called;
      expect(authToken).to.be.null;
    });

    it('extends expiry of an unexpired authentication token', async () => {
      const now = Date.now();
      const token = {
        user_id: '1234',
        perms: {
          'course/2345': ['read', 'write'],
          'topic/12345/course': ['read', 'write'],
        },
        expiry: now + 60000,
      };
      const authHeader = 'Bearer ' + encodeAuthToken(token);
      const authToken = await createAuthToken({ authHeader });

      expect(authToken.expiry).to.gte(now + 600000);
      expect(authToken).to.deep.equal({ ...token, expiry: authToken.expiry });
    });

    it('does not extend expiry of an expired authentication token', async () => {
      const now = Date.now();
      const expiredToken = {
        user_id: '1234',
        perms: {
          'course/2345': ['read', 'write'],
          'topic/12345/course': ['read', 'write'],
        },
        expiry: now - 1,
      };
      const authHeader = 'Bearer ' + encodeAuthToken(expiredToken);
      const authToken = await createAuthToken({ authHeader });

      expect(authToken).to.be.null;
    });

    it('throws when an invalid authentication token is given', async () => {
      const authHeader = 'Bearer abcd';
      let error = null;

      try {
        await createAuthToken({ authHeader });
      } catch (err) {
        error = err;
      }

      assert(error, 'expected createAuthToken to have thrown');
    });
  });

  describe('authenticate', () => {
    it('sets X-Auth-Token response header', async () => {
      const ctx = {
        header: {},
        request: {
          get: stub().returns('Basic am9obkBleGFtcGxlLmNvbTpzZWNyZXQ='),
        },
        response: {
          set: stub(),
        },
        throw: stub().throws(),
      };
      const crud = {
        user: {
          read: stub().resolves([{ _id: 1234 }]),
        },
        perm: {
          read: stub().resolves([{
            user_id: '1234',
            perms: {
              'course/2345': ['read', 'write'],
              'topic/12345/course': ['read', 'write'],
            },
          }]),
        },
      };
      const nowStub = stub(Date, 'now').returns(1000000);
      const authToken = await authenticate({ ctx, crud });

      nowStub.restore();

      expect(ctx.request.get).to.have.been.calledOnceWith('Authorization');
      expect(crud.user.read).to.have.been.calledOnceWith({
        email: 'john@example.com',
        passcode: 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4',
        enabled: true,
      });
      expect(crud.perm.read).to.have.been.calledOnceWith({ user_id: 1234 });
      expect(ctx.throw).to.not.have.been.called;
      expect(authToken).to.deep.equal({
        user_id: '1234',
        perms: {
          'course/2345': ['read', 'write'],
          'topic/12345/course': ['read', 'write'],
        },
        expiry: 1600000,
      });
      expect(ctx.response.set).to.have.been.calledOnceWith('X-Auth-Token', encodeAuthToken(authToken));
      expect(ctx.header).to.not.have.property('WWW-Authenticate');
    });

    it('responds with HTTP 401 when token cannot be created', async () => {
      const ctx = {
        header: {},
        request: {
          get: stub(),
        },
        response: {
          set: stub(),
        },
        throw: stub().throws(),
      };
      const crud = {
        user: {
          read: stub().resolves(),
        },
        perm: {
          read: stub().resolves(),
        },
      };
      const createToken = stub().resolves();
      let error;

      try {
        await authenticate({ ctx, crud, createToken });
      } catch (err) {
        error = err;
      }

      assert(error, 'expected authenticate to have thrown');
      expect(createToken).to.have.been.calledOnceWith({ crud, authHeader: '' });
      expect(ctx.request.get).to.have.been.calledOnceWith('Authorization');
      expect(crud.user.read).to.not.have.been.called;
      expect(crud.perm.read).to.not.have.been.called;
      expect(ctx.throw).to.have.been.calledOnceWith(401, 'Unauthorized');
      expect(ctx.header).to.have.property('WWW-Authenticate').that.is.a('string');
      expect(ctx.header['WWW-Authenticate']).to.equal('Basic realm="This Website", charset="UTF-8"');
    });
  });
});
