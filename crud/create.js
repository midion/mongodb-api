const auth = require('./auth');
const helpers = require('./helpers');

const create = async (ctx, kind, dependencies) => {
  const { authenticate = auth.authenticate, createCRUD, crud, realm } = dependencies;
  const authToken = await authenticate({ ctx, crud, realm });
  const reqPerms = ['create', 'write'];
  const document = ctx.request.body;
  const directAllowed = helpers.isDirectAllowed(kind, authToken.perms, reqPerms);
  const allowed = directAllowed || helpers.isAllowed(kind, document, authToken.perms, reqPerms);

  ctx.assert(allowed, 403, 'Forbidden');
  crud[kind] = kind in crud ? crud[kind] : createCRUD(kind);
  ctx.body = await crud[kind].create(document);
  ctx.status = 201;
};

const createUnderParent = async (ctx, pKind, pId, kind, dependencies) => {
  ctx.request.body[`${pKind}_id`] = pId;
  await create(ctx, kind, dependencies);
};

module.exports = { create, createUnderParent };
