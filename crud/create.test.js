const { afterEach, describe, it } = require('mocha');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const { create, createUnderParent } = require('./create');

chai.use(sinonChai);

describe('api/crud/create', () => {
  const { expect } = chai;
  const { stub, match: { any } } = sinon;
  const ctxAssertStub = stub();
  const ctxThrowStub = stub().throws();
  const ctxRequestGetStub = stub();

  ctxAssertStub.withArgs(true, any, any).returns();
  ctxAssertStub.withArgs(false, any, any).throws('AssertError');
  ctxRequestGetStub.withArgs('Authorization').returns('Basic am9obkBleGFtcGxlLmNvbTpzZWNyZXQ=');

  afterEach(() => {
    ctxAssertStub.resetHistory();
    ctxThrowStub.resetHistory();
    ctxRequestGetStub.resetHistory();
  });

  describe('create', () => {
    it('creates document when direct allowed', async () => {
      const authenticate = stub().resolves({
        perms: {
          'user': ['write'],
        },
      });
      const crud = {
        user: {
          create: stub().resolves('12345'),
        },
      };
      const user = { email: 'john@example', org_id: '54321' };
      const ctx = {
        assert: ctxAssertStub,
        request: {
          body: user,
        },
      };
      await create(ctx, 'user', { authenticate, crud });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: undefined });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(true, 403, 'Forbidden');
      expect(crud.user.create).to.have.been.calledOnceWithExactly(user);
      expect(ctx.body).to.equal('12345');
      expect(ctx.status).to.equal(201);
    });
    it('creates document when allowed to have specific parents', async () => {
      const authenticate = stub().resolves({
        perms: {
          'org/54321/user': ['write'],
          'group/6789/user': ['write'],
        },
      });
      const crud = {
        user: {
          create: stub().resolves('12345'),
        },
      };
      const user = { email: 'john@example', org_id: '54321', group_id: '6789' };
      const ctx = {
        assert: ctxAssertStub,
        request: {
          body: user,
        },
      };
      await create(ctx, 'user', { authenticate, crud, realm: 'Example' });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: 'Example' });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(true, 403, 'Forbidden');
      expect(crud.user.create).to.have.been.calledOnceWithExactly(user);
      expect(ctx.body).to.equal('12345');
      expect(ctx.status).to.equal(201);
    });
    it('throws when not allowed to have a specific parent', async () => {
      const authenticate = stub().resolves({
        perms: {
          'org/54321/user': ['write'],
          'group/6789/user': ['read'],
        },
      });
      const crud = {
        user: {
          create: stub().resolves('12345'),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
        request: {
          body: { email: 'john@example', org_id: '54321', group_id: '6789' },
        },
      };
      let error;

      try {
        await create(ctx, 'user', { authenticate, crud, realm: 'Example' });
      } catch (err) {
        error = err;
      }

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: 'Example' });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(false, 403, 'Forbidden');
      expect(crud.user.create).to.not.have.been.called;
      expect(error).to.have.property('name').that.is.equal('AssertError');
    });
    it('creates document when CRUD does not even exist yet', async () => {
      const authenticate = stub().resolves({
        perms: {
          'user': ['write'],
        },
      });
      const createCRUD = stub().returns({ create: stub().resolves('12345') });
      const crud = { _: 'crud' };
      const user = { email: 'john@example', org_id: '54321' };
      const ctx = {
        assert: ctxAssertStub,
        request: {
          body: user,
        },
      };
      await create(ctx, 'user', { authenticate, createCRUD, crud });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: undefined });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(true, 403, 'Forbidden');
      expect(createCRUD).to.have.been.calledOnceWithExactly('user');
      expect(crud).to.have.property('user').that.is.a('object');
      expect(crud.user).to.have.property('create').that.is.a('function');
      expect(crud.user.create).to.have.been.calledOnceWithExactly(user);
      expect(ctx.body).to.equal('12345');
      expect(ctx.status).to.equal(201);
    });
  });
  describe('createUnderParent', () => {
    it('creates document when direct allowed', async () => {
      const authenticate = stub().resolves({
        perms: {
          'user': ['write'],
        },
      });
      const crud = {
        user: {
          create: stub().resolves('12345'),
        },
      };
      const user = { email: 'john@example' };
      const ctx = {
        assert: ctxAssertStub,
        request: {
          body: user,
        },
      };
      await createUnderParent(ctx, 'org', '54321', 'user', { authenticate, crud });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: undefined });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(true, 403, 'Forbidden');
      expect(crud.user.create).to.have.been.calledOnceWithExactly({ ...user, org_id: '54321' });
      expect(ctx.body).to.equal('12345');
      expect(ctx.status).to.equal(201);
    });
    it('creates document when allowed to have specific parent', async () => {
      const crud = {
        user: {
          read: stub().resolves([{ _id: 1234 }]),
          create: stub().resolves('12345'),
        },
        perm: {
          read: stub().resolves([{
            perms: {
              'group/6789/user': ['read', 'write'],
            },
          }]),
        },
      };
      const user = { email: 'john@example' };
      const ctx = {
        assert: ctxAssertStub,
        header: {},
        throw: ctxThrowStub,
        request: {
          body: user,
          get: ctxRequestGetStub,
        },
        response: {
          set: stub(),
        },
      };

      await createUnderParent(ctx, 'group', '6789', 'user', { crud });

      expect(ctx.request.get).to.have.been.calledOnceWithExactly('Authorization');
      expect(ctx.response.set).to.have.been.calledOnceWith('X-Auth-Token');
      expect(ctx.throw).to.not.have.been.called;
      expect(ctx.header).to.not.have.property('WWW-Authenticate');

      expect(ctx.assert).to.have.been.calledOnceWithExactly(true, 403, 'Forbidden');
      expect(crud.user.create).to.have.been.calledOnceWithExactly({ ...user, group_id: '6789' });
      expect(ctx.body).to.equal('12345');
      expect(ctx.status).to.equal(201);
    });
    it('throws when not allowed to have parent', async () => {
      const authenticate = stub().resolves({
        perms: {
          'org/6789/user': ['write'],
        },
      });
      const crud = {
        user: {
          create: stub().resolves('12345'),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
        request: {
          body: { email: 'john@example' },
        },
      };
      let error;

      try {
        await createUnderParent(ctx, 'org', '54321', 'user', { authenticate, crud, realm: 'Example' });
      } catch (err) {
        error = err;
      }

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: 'Example' });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(false, 403, 'Forbidden');
      expect(crud.user.create).to.not.have.been.called;
      expect(error).to.have.property('name').that.is.equal('AssertError');
    });
  });
});
