const auth = require('./auth');
const helpers  = require('./helpers');

const deleteOne = async (ctx, kind, id, dependencies) => {
  const { authenticate = auth.authenticate, createCRUD, crud, realm } = dependencies;
  const authToken = await authenticate({ ctx, crud, realm });
  const reqPerms = ['delete', 'write'];
  const directAllowed = helpers.isDirectAllowed(kind, authToken.perms, reqPerms);
  const potentiallyAllowed = !directAllowed && helpers.isPotentiallyAllowed(kind, authToken.perms, reqPerms);
  let allowed, document;

  ctx.assert(directAllowed || potentiallyAllowed, 403, 'Forbidden');
  crud[kind] = kind in crud ? crud[kind] : createCRUD(kind);
  document = await crud[kind].read(id);
  ctx.assert(!!document, 404, 'Not Found');
  allowed = directAllowed || helpers.isAllowed(kind, document, authToken.perms, reqPerms);
  ctx.assert(allowed, 403, 'Forbidden');
  await crud[kind].delete(id);
  ctx.status = 204;
};

module.exports = { deleteOne };
