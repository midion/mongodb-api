const { afterEach, describe, it } = require('mocha');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const { deleteOne } = require('./delete');

chai.use(sinonChai);

describe('api/crud/delete', () => {
  const { expect } = chai;
  const { stub, match: { any } } = sinon;
  const ctxAssertStub = stub();
  const ctxThrowStub = stub().throws();
  const ctxRequestGetStub = stub();

  ctxAssertStub.withArgs(true, any, any).returns();
  ctxAssertStub.withArgs(false, any, any).throws('AssertError');
  ctxRequestGetStub.withArgs('Authorization').returns('Basic am9obkBleGFtcGxlLmNvbTpzZWNyZXQ=');

  afterEach(() => {
    ctxAssertStub.resetHistory();
    ctxThrowStub.resetHistory();
    ctxRequestGetStub.resetHistory();
  });

  describe('deleteOne', () => {
    it('deletes document when direct allowed', async () => {
      const authenticate = stub().resolves({
        perms: {
          'user': ['write'],
        },
      });
      const user = { email: 'john@example', org_id: '54321' };
      const crud = {
        user: {
          read: stub().resolves(user),
          delete: stub().resolves(1),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
      };
      await deleteOne(ctx, 'user', '123', { authenticate, crud, realm: 'REALM' });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: 'REALM' });
      expect(ctx.assert).to.have.been.calledThrice;
      expect(ctx.assert).to.have.been.calledWithExactly(true, 403, 'Forbidden');
      expect(ctx.assert).to.have.been.calledWithExactly(true, 404, 'Not Found');
      expect(ctx.assert).to.not.have.been.calledWith(false);
      expect(crud.user.read).to.have.been.calledOnceWithExactly('123');
      expect(crud.user.delete).to.have.been.calledOnceWithExactly('123');
      expect(ctx.status).to.equal(204);
    });
    it('deletes document when direct allowed and CRUD does not exist', async () => {
      const authenticate = stub().resolves({
        perms: {
          'user': ['write'],
        },
      });
      const user = { email: 'john@example', org_id: '54321' };
      const createCRUD = stub().returns({
        read: stub().resolves(user),
        delete: stub().resolves(1),
      });
      const crud = { _: 'crud' };
      const ctx = {
        assert: ctxAssertStub,
      };
      await deleteOne(ctx, 'user', '123', { authenticate, createCRUD, crud, realm: 'REALM' });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: 'REALM' });
      expect(createCRUD).to.have.been.calledOnceWithExactly('user');
      expect(ctx.assert).to.have.been.calledThrice;
      expect(ctx.assert).to.have.been.calledWithExactly(true, 403, 'Forbidden');
      expect(ctx.assert).to.have.been.calledWithExactly(true, 404, 'Not Found');
      expect(ctx.assert).to.not.have.been.calledWith(false);
      expect(crud.user.read).to.have.been.calledOnceWithExactly('123');
      expect(crud.user.delete).to.have.been.calledOnceWithExactly('123');
      expect(ctx.status).to.equal(204);
    });
    it('deletes document when allowed via parents', async () => {
      const users = [
        { _id: '1', email: 'john@example', org_id: '54321' },
        { _id: '2', email: 'bart@example', org_id: '54321' },
      ];
      const user = { email: 'john@example', org_id: '54321', group_id: '1234' };
      const crud = {
        user: {
          read: ((stub) => (
            stub.withArgs('123').resolves(user),
            stub.resolves(users),
            stub
          ))(stub()),
          delete: stub().resolves(1),
        },
        perm: {
          read: stub().resolves([{
            perms: {
              'org/54321/user': ['delete'],
              'group/1234/user': ['write'],
            },
          }]),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
        header: {},
        request: {
          get: ctxRequestGetStub,
        },
        response: {
          set: stub(),
        },
        throw: ctxThrowStub,
      };

      await deleteOne(ctx, 'user', '123', { crud, realm: 'REALM' });

      expect(ctx.request.get).to.have.been.calledOnceWithExactly('Authorization');
      expect(ctx.response.set).to.have.been.calledOnceWith('X-Auth-Token');
      expect(ctx.throw).to.not.have.been.called;
      expect(ctx.header).to.not.have.property('WWW-Authenticate');
      expect(ctx.assert).to.have.been.calledThrice;
      expect(ctx.assert).to.have.been.calledWithExactly(true, 403, 'Forbidden');
      expect(ctx.assert).to.have.been.calledWithExactly(true, 404, 'Not Found');
      expect(ctx.assert).to.not.have.been.calledWith(false);
      expect(crud.user.read).to.have.been.calledTwice;
      expect(crud.user.read).to.have.been.calledWithExactly({
        email: 'john@example.com',
        enabled: true,
        passcode: 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4',
      });
      expect(crud.user.read).to.have.been.calledWithExactly('123');
      expect(crud.user.delete).to.have.been.calledOnceWithExactly('123');
      expect(ctx.status).to.equal(204);
    });
    it('throws 403 when not even potentially allowed', async () => {
      const authenticate = stub().resolves({
        perms: {
          'org': ['delete'],
        },
      });
      const user = { email: 'john@example', org_id: '54321', group_id: '1234' };
      const crud = {
        user: {
          read: stub().resolves(user),
          delete: stub().resolves(1),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
      };
      let error;

      try {
        await deleteOne(ctx, 'user', '123', { authenticate, crud, realm: 'REALM' });
      } catch (err) {
        error = err;
      }

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: 'REALM' });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(false, 403, 'Forbidden');
      expect(crud.user.read).to.not.have.been.called;
      expect(crud.user.delete).to.not.have.been.called;
      expect(error).to.have.property('name').that.is.equal('AssertError');
    });
    it('throws 403 when potentially but not really allowed', async () => {
      const authenticate = stub().resolves({
        perms: {
          'org/123/user': ['delete'],
        },
      });
      const user = { email: 'john@example', org_id: '54321', group_id: '1234' };
      const crud = {
        user: {
          read: stub().resolves(user),
          delete: stub().resolves(1),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
      };
      let error;

      try {
        await deleteOne(ctx, 'user', '234', { authenticate, crud, realm: 'REALM' });
      } catch (err) {
        error = err;
      }

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: 'REALM' });
      expect(ctx.assert).to.have.been.calledThrice;
      expect(ctx.assert).to.have.been.calledWithExactly(true, 403, 'Forbidden');
      expect(ctx.assert).to.have.been.calledWithExactly(true, 404, 'Not Found');
      expect(ctx.assert).to.have.been.calledWithExactly(false, 403, 'Forbidden');
      expect(crud.user.read).to.have.been.calledOnceWithExactly('234');
      expect(crud.user.delete).to.not.have.been.called;
      expect(error).to.have.property('name').that.is.equal('AssertError');
    });
    it('throws 404 when potentially allowed but document does not exist', async () => {
      const authenticate = stub().resolves({
        perms: {
          'org/123/user': ['delete'],
        },
      });
      const crud = {
        user: {
          read: stub().resolves(null),
          delete: stub().resolves(1),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
      };
      let error;

      try {
        await deleteOne(ctx, 'user', '234', { authenticate, crud, realm: 'REALM' });
      } catch (err) {
        error = err;
      }

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: 'REALM' });
      expect(ctx.assert).to.have.been.calledTwice;
      expect(ctx.assert).to.have.been.calledWithExactly(true, 403, 'Forbidden');
      expect(ctx.assert).to.have.been.calledWithExactly(false, 404, 'Not Found');
      expect(crud.user.read).to.have.been.calledOnceWithExactly('234');
      expect(crud.user.delete).to.not.have.been.called;
      expect(error).to.have.property('name').that.is.equal('AssertError');
    });
  });
});
