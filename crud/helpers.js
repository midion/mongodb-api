const containsAnyOf = (array = [], values) => array.some(value => values.includes(value));

const isDirectAllowed = (kind, actualPerms, reqPerms) => containsAnyOf(actualPerms[kind], reqPerms);

const isAllowed = (kind, docs, actualPerms, reqPerms) => (Array.isArray(docs) ? docs : [docs])
  .reduce((foreignProps, doc) => foreignProps.concat(Object.entries(doc).filter(([key]) => /._id$/.test(key))), [])
  .map(([key, val]) => `${key.slice(0, -3)}/${val}/${kind}`)
  .every(uri => containsAnyOf(actualPerms[uri], reqPerms));

const isPotentiallyAllowed = (kind, actualPerms, reqPerms, regex = new RegExp(`(?:^|/)${kind}$`)) =>
  Object.entries(actualPerms).some(([uri, perms]) => regex.test(uri) && containsAnyOf(perms, reqPerms));

module.exports = { containsAnyOf, isDirectAllowed, isAllowed, isPotentiallyAllowed };
