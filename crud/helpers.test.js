const { describe, it } = require('mocha');
const chai = require('chai');
const { containsAnyOf, isAllowed, isDirectAllowed, isPotentiallyAllowed } = require('./helpers');

describe('api/crud/helpers', () => {
  const { assert, expect } = chai;

  describe('containsAnyOf', () => {
    it('returns true when array contains one of the values', () => {
      expect(containsAnyOf(['A', 'B'], ['A'])).to.be.true;
    });
    it('returns true when array contains some of the values', () => {
      expect(containsAnyOf(['A', 'B', 'C'], ['B', 'A'])).to.be.true;
    });
    it('returns true when array contains all the values', () => {
      expect(containsAnyOf(['A', 'B', 'C'], ['B', 'C', 'A'])).to.be.true;
    });
    it('returns false when array does not contain any of the values', () => {
      expect(containsAnyOf(['A', 'B', 'C'], ['X', 'Y', 'Z'])).to.be.false;
    });
    it('returns false when values is empty', () => {
      expect(containsAnyOf(['A', 'B', 'C'], [])).to.be.false;
    });
    it('returns false when array is empty', () => {
      expect(containsAnyOf([], ['A'])).to.be.false;
    });
    it('returns false when called without arguments', () => {
      expect(containsAnyOf()).to.be.false;
    });
    it('throws if array is not an array', () => {
      let error;

      try {
        containsAnyOf('array', []);
      } catch (err) {
        error = err;
      }
      assert(error, 'expected containsAnyOf to have thrown');
    });
    it('throws if array is not empty and values is not an array', () => {
      let error;

      try {
        containsAnyOf(['A']);
      } catch (err) {
        error = err;
      }
      assert(error, 'expected containsAnyOf to have thrown');
    });
  });

  describe('isDirectAllowed', () => {
    it('returns true when actual permissions contain any of the requested permissions', () => {
      expect(isDirectAllowed('kind', { kind: ['read', 'write'] }, ['read'])).to.be.true;
    });
    it('returns false when actual permissions contain none of the requested permissions', () => {
      expect(isDirectAllowed('kind', { kind: ['read', 'write'] }, ['update', 'delete'])).to.be.false;
    });
  });

  describe('isAllowed', () => {
    it('returns true when actual permissions contain any of the requested permission for all foreign properties in the document', () => {
      const document = { _id: '123', org_id: '1', group_id: '2' };
      const actualPerms = {
        'org/1/kind': ['read', 'write'],
        'group/2/kind': ['read', 'write'],
      };
      expect(isAllowed('kind', document, actualPerms, ['read'])).to.be.true;
    });
    it('returns false when not for all foreign properties in the document actual permissions contain any of the requested permission', () => {
      const document = { _id: '123', org_id: '1', group_id: '2' };
      const actualPerms = {
        'org/1/kind': ['read', 'write'],
      };
      expect(isAllowed('kind', document, actualPerms, ['write'])).to.be.false;
    });
  });

  describe('isPotentiallyAllowed', () => {
    it('returns true when actual permissions contain any of the requested permission for any foreign properties', () => {
      const actualPerms = {
        'org/5/kind': ['read', 'write'],
        'group/3/kind': ['read', 'write'],
      };
      expect(isPotentiallyAllowed('kind', actualPerms, ['read'])).to.be.true;
    });
    it('returns false when actual permissions do not contain any of the requested permission', () => {
      const actualPerms = {
        'org/5/another-kind': ['read', 'write'],
      };
      expect(isPotentiallyAllowed('kind', actualPerms, ['read'])).to.be.false;
    });
  });
});
