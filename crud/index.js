const createExports = require('./create');
const readExports = require('./read');
const updateExports = require('./update');
const deleteExports = require('./delete');
const middlewaresExports = require('./middlewares');

module.exports = {
  ...createExports,
  ...readExports,
  ...updateExports,
  ...deleteExports,
  ...middlewaresExports,
};
