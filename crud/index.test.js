const { describe, it } = require('mocha');
const chai = require('chai');
const createExports = require('./create');
const readExports = require('./read');
const updateExports = require('./update');
const deleteExports = require('./delete');
const middlewaresExports = require('./middlewares');
const ownExports = require('./');

describe('api/crud/index', () => {
  const { expect } = chai;

  it('exports everything from create', () => {
    expect(Object.values(ownExports)).to.include.members(Object.values(createExports));
  });
  it('exports everything from read', () => {
    expect(Object.values(ownExports)).to.include.members(Object.values(readExports));
  });
  it('exports everything from update', () => {
    expect(Object.values(ownExports)).to.include.members(Object.values(updateExports));
  });
  it('exports everything from delete', () => {
    expect(Object.values(ownExports)).to.include.members(Object.values(deleteExports));
  });
  it('exports everything from middlewares', () => {
    expect(Object.values(ownExports)).to.include.members(Object.values(middlewaresExports));
  });
});
