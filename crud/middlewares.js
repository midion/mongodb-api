const koaBodyParser = require('koa-bodyparser');
const koaRoute = require('koa-route');
const mongodbCrud = require('@midion/mongodb-crud');
const apiAuth = require('./auth');
const apiCreate = require('./create');
const apiRead = require('./read');
const apiUpdate = require('./update');
const apiDelete = require('./delete');

const defaultAllowOriginMiddleware = async (ctx, next) => {
  const origin = ctx.request.get('Origin');
  if (origin) {
    ctx.set('Access-Control-Allow-Origin', origin);
  }
  await next();
};

const middlewares = ({
  baseURI = '/api',
  connectionOptions,
  database,
  dependencies: {
    allowOriginMiddleware = defaultAllowOriginMiddleware,
    bodyParserMiddleware = koaBodyParser(),
    connector = mongodbCrud.createConnector(connectionOptions),
    createCRUD = kind => mongodbCrud.createCRUD(connector, database, kind),
    crud = {
      perm: createCRUD('perm'),
      user: createCRUD('user'),
    },
    authenticate = apiAuth.authenticate,
    create = apiCreate.create,
    createUnderParent = apiCreate.createUnderParent,
    read = apiRead.read,
    readOne = apiRead.readOne,
    readUnderParent = apiRead.readUnderParent,
    replaceOne = apiUpdate.replaceOne,
    updateOne = apiUpdate.updateOne,
    deleteOne = apiDelete.deleteOne,
    httpOptions = koaRoute.options,
    httpGet = koaRoute.get,
    httpPost = koaRoute.post,
    httpDelete = koaRoute.del,
    httpPut = koaRoute.put,
    httpPatch = koaRoute.patch,
  } = {},
  realm,
}) => [
  allowOriginMiddleware,
  bodyParserMiddleware,
  httpOptions(`${baseURI}/*`, ctx => {
    ctx.set({
      'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE',
      'Access-Control-Allow-Headers': 'Authorization, Content-Type, X-Requested-With',
    });
    ctx.status = 204;
  }),
  httpPost(`${baseURI}/auth`, async ctx => {
    await authenticate(ctx);
    ctx.status = 204;
  }),
  httpPost(`${baseURI}/:kind`, async (ctx, kind) => {
    await create(ctx, kind, { authenticate, createCRUD, crud, realm });
  }),
  httpPost(`${baseURI}/:parent/:id/:kind`, async (ctx, pKind, pId, kind) => {
    await createUnderParent(ctx, pKind, pId, kind, { authenticate, createCRUD, crud, realm });
  }),
  httpGet(`${baseURI}/:kind`, async (ctx, kind) => {
    await read(ctx, kind, { authenticate, createCRUD, crud, realm });
  }),
  httpGet(`${baseURI}/:parent/:id/:kind`, async (ctx, pKind, pId, kind) => {
    await readUnderParent(ctx, pKind, pId, kind, { authenticate, createCRUD, crud, realm });
  }),
  httpGet(`${baseURI}/:kind/:id`, async (ctx, kind, id) => {
    await readOne(ctx, kind, id, { authenticate, createCRUD, crud, realm });
  }),
  httpDelete(`${baseURI}/:kind/:id`, async (ctx, kind, id) => {
    await deleteOne(ctx, kind, id, { authenticate, createCRUD, crud, realm });
  }),
  httpPut(`${baseURI}/:kind/:id`, async (ctx, kind, id) => {
    await replaceOne(ctx, kind, id, { authenticate, createCRUD, crud, realm });
  }),
  httpPatch(`${baseURI}/:kind/:id`, async (ctx, kind, id) => {
    await updateOne(ctx, kind, id, { authenticate, createCRUD, crud, realm });
  }),
];

module.exports = { defaultAllowOriginMiddleware, middlewares };

