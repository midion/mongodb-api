const { afterEach, describe, it } = require('mocha');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const koaRoute = require('koa-route');
const mongodbCrud = require('@midion/mongodb-crud');
const apiAuth = require('./auth');
const apiCreate = require('./create');
const apiRead = require('./read');
const apiUpdate = require('./update');
const apiDelete = require('./delete');
const { middlewares, defaultAllowOriginMiddleware } = require('./middlewares');

chai.use(sinonChai);

describe('api/crud/middlewares', () => {
  const { expect } = chai;
  const { spy, stub } = sinon;

  const allowOriginMiddleware = stub();
  const bodyParserMiddleware = stub();
  const crudStub = {
    create: stub().resolves(),
    read: stub().resolves(),
    update: stub().resolves(),
    delete: stub().resolves(),
  };
  const createCRUD = stub().returns(crudStub);
  const authenticate = stub().resolves();
  const middlewareStubs = {};
  const httpDelete = spy((route, callback) => (middlewareStubs.deleteOne = stub().callsFake(callback)));

  const httpGet = spy((route, callback) => {
    const middleware = stub().callsFake(callback);
    switch (route) {
    case '/my-generic-api/:kind':
      middlewareStubs.read = middleware;
      break;
    case '/my-generic-api/:kind/:id':
      middlewareStubs.readOne = middleware;
      break;
    case '/my-generic-api/:parent/:id/:kind':
      middlewareStubs.readUnderParent = middleware;
      break;
    }
    return middleware;
  });
  const httpOptions = spy((route, callback) => (middlewareStubs.options = stub().callsFake(callback)));
  const httpPatch = spy((route, callback) => (middlewareStubs.updateOne = stub().callsFake(callback)));
  const httpPut = spy((route, callback) => (middlewareStubs.replaceOne = stub().callsFake(callback)));
  const httpPost = spy((route, callback) => {
    const middleware = stub().callsFake(callback);
    switch (route) {
    case '/my-generic-api/auth':
      middlewareStubs.auth = middleware;
      break;
    case '/my-generic-api/:kind':
      middlewareStubs.create = middleware;
      break;
    case '/my-generic-api/:parent/:id/:kind':
      middlewareStubs.createUnderParent = middleware;
      break;
    }
    return middleware;
  });
  const create = stub().resolves();
  const createUnderParent = stub().resolves();
  const read = stub().resolves();
  const readOne = stub().resolves();
  const readUnderParent = stub().resolves();
  const replaceOne = stub().resolves();
  const updateOne = stub().resolves();
  const deleteOne = stub().resolves();

  const crud = { _: 'crud', perm: stub().resolves(), user: stub().resolves(), };

  const stubs = [
    allowOriginMiddleware,
    bodyParserMiddleware,
    ...Object.values(crudStub),
    createCRUD,
    crud.perm,
    crud.user,
    authenticate,
    ...Object.values(middlewareStubs),
    httpDelete,
    httpGet,
    httpOptions,
    httpPatch,
    httpPut,
    httpPost,
    create,
    createUnderParent,
    read,
    readOne,
    readUnderParent,
    replaceOne,
    updateOne,
    deleteOne,
  ];

  afterEach(() => {
    stubs.forEach(stub => stub.resetHistory());
  });

  const getMiddlewares = ({
    baseURI = '/my-generic-api',
    connectionOptions,
    database = 'test-db',
    dependencies = {},
    realm = 'REALM',
    ...rest
  } = {}) => middlewares({
    baseURI: baseURI || undefined,
    connectionOptions,
    database,
    dependencies: {
      allowOriginMiddleware,
      bodyParserMiddleware,
      createCRUD,
      crud,
      authenticate,
      create,
      createUnderParent,
      read,
      readOne,
      readUnderParent,
      replaceOne,
      updateOne,
      deleteOne,
      httpDelete,
      httpGet,
      httpOptions,
      httpPatch,
      httpPost,
      httpPut,
      ...dependencies,
    },
    realm,
    ...rest,
  });

  it('returns an array of middlewares', () => {
    const connector = { _: 'connector' };
    const connectionOptions = { _: 'connectionOptions' };
    const createConnectorStub = stub(mongodbCrud, 'createConnector').returns(connector);
    const createCRUDStub = stub(mongodbCrud, 'createCRUD');

    expect(middlewares({ connectionOptions, database: 'test-db' })).to.be.an('array');
    expect(createConnectorStub).to.have.been.calledOnceWithExactly(connectionOptions);
    expect(createCRUDStub).to.have.been.calledWithExactly(connector, 'test-db', 'perm');
    expect(createCRUDStub).to.have.been.calledWithExactly(connector, 'test-db', 'user');

    createConnectorStub.restore();
    createCRUDStub.restore();
  });

  it('sets Access-Control-Allow-Origin header when Origin header is present in request', async () => {
    const ctx = {
      _: 'ctx',
      request: {
        get: stub().returns('example.com'),
      },
      set: stub(),
    };
    const next = stub().resolves();
    await defaultAllowOriginMiddleware(ctx, next);
    expect(ctx.request.get).to.have.been.calledOnceWithExactly('Origin');
    expect(ctx.set).to.have.been.calledOnceWithExactly('Access-Control-Allow-Origin', 'example.com');
    expect(next).to.have.been.calledOnceWithExactly();
  });
  it('does not set Access-Control-Allow-Origin header when Origin header is not present in request', async () => {
    const ctx = {
      _: 'ctx',
      request: {
        get: stub().returns(null),
      },
      set: stub(),
    };
    const next = stub().resolves();
    await defaultAllowOriginMiddleware(ctx, next);
    expect(ctx.request.get).to.have.been.calledOnceWithExactly('Origin');
    expect(ctx.set).to.not.have.been.called;
    expect(next).to.have.been.calledOnceWithExactly();
  });
  it('creates CRUD when a custom one is not given', () => {
    const options = { dependencies: { crud: undefined } };
    getMiddlewares(options);
    expect(createCRUD).to.have.been.calledWithExactly('perm');
    expect(createCRUD).to.have.been.calledWithExactly('user');
  });
  it('creates CRUD with default createCRUD when a custom one is not given', async () => {
    let createDeps;
    const ctx = { _: 'ctx' };
    const options = {
      dependencies: {
        createCRUD: undefined,
        crud: undefined,
        create: spy((ctx, kind, deps) => {
          createDeps = deps;
          return Promise.resolve();
        }),
      },
    };
    getMiddlewares(options);
    await middlewareStubs.create(ctx, 'kind');
    expect(options.dependencies.create).to.have.been.calledOnceWith(ctx, 'kind');
    expect(createDeps).to.have.property('createCRUD').that.is.not.equal(createCRUD);
    expect(createDeps).to.have.property('crud').that.is.not.equal(createCRUD);
  });
  it('calls http methods from koa-route when custom ones are not provided', () => {
    const options = {
      dependencies: {
        httpOptions: undefined,
        httpGet: undefined,
        httpPost: undefined,
        httpDelete: undefined,
        httpPut: undefined,
        httpPatch: undefined,
      },
    };
    const koaRouteStubs = [
      stub(koaRoute, 'options'),
      stub(koaRoute, 'get'),
      stub(koaRoute, 'post'),
      stub(koaRoute, 'del'),
      stub(koaRoute, 'put'),
      stub(koaRoute, 'patch'),
    ];

    getMiddlewares(options);

    expect(httpOptions).to.not.have.been.called;
    expect(httpGet).to.not.have.been.called;
    expect(httpPost).to.not.have.been.called;
    expect(httpDelete).to.not.have.been.called;
    expect(httpPut).to.not.have.been.called;
    expect(httpPatch).to.not.have.been.called;

    expect(koaRoute.options).to.have.been.called;
    expect(koaRoute.get).to.have.been.called;
    expect(koaRoute.post).to.have.been.called;
    expect(koaRoute.del).to.have.been.called;
    expect(koaRoute.put).to.have.been.called;
    expect(koaRoute.patch).to.have.been.called;

    koaRouteStubs.forEach(stub => stub.restore());
  });
  it('calls default api methods when custom ones are not provided', () => {
    const options = {
      dependencies: {
        authenticate: undefined,
        create: undefined,
        createUnderParent: undefined,
        read: undefined,
        readOne: undefined,
        readUnderParent: undefined,
        replaceOne: undefined,
        updateOne: undefined,
        deleteOne: undefined,
      },
    };
    const apiStubs = [
      stub(apiAuth, 'authenticate'),
      stub(apiCreate, 'create'),
      stub(apiCreate, 'createUnderParent'),
      stub(apiRead, 'read'),
      stub(apiRead, 'readOne'),
      stub(apiRead, 'readUnderParent'),
      stub(apiUpdate, 'replaceOne'),
      stub(apiUpdate, 'updateOne'),
      stub(apiDelete, 'deleteOne'),
    ];
    const ctx = { _: 'ctx' };

    getMiddlewares(options);
    middlewareStubs.auth(ctx);
    middlewareStubs.create(ctx, 'kind');
    middlewareStubs.createUnderParent(ctx, 'parent', 'id', 'kind');
    middlewareStubs.read(ctx, 'kind');
    middlewareStubs.readUnderParent(ctx, 'parent', 'id', 'kind');
    middlewareStubs.readOne(ctx, 'kind', 'id');
    middlewareStubs.replaceOne(ctx, 'kind', 'id');
    middlewareStubs.updateOne(ctx, 'kind', 'id');
    middlewareStubs.deleteOne(ctx, 'kind', 'id');

    expect(authenticate).to.not.have.been.called;
    expect(create).to.not.have.been.called;
    expect(createUnderParent).to.not.have.been.called;
    expect(read).to.not.have.been.called;
    expect(readOne).to.not.have.been.called;
    expect(readUnderParent).to.not.have.been.called;
    expect(replaceOne).to.not.have.been.called;
    expect(updateOne).to.not.have.been.called;
    expect(deleteOne).to.not.have.been.called;

    expect(apiAuth.authenticate).to.have.been.called;
    expect(apiCreate.create).to.have.been.called;
    expect(apiCreate.createUnderParent).to.have.been.called;
    expect(apiRead.read).to.have.been.called;
    expect(apiRead.readOne).to.have.been.called;
    expect(apiRead.readUnderParent).to.have.been.called;
    expect(apiUpdate.replaceOne).to.have.been.called;
    expect(apiUpdate.updateOne).to.have.been.called;
    expect(apiDelete.deleteOne).to.have.been.called;

    apiStubs.forEach(stub => stub.restore());
  });
  it('uses default baseURI when a custom one is not given', () => {
    getMiddlewares({ baseURI: null });
    expect(httpOptions).to.have.been.calledWith('/api/*');
    expect(httpPost).to.have.been.calledWith('/api/auth');
    expect(httpPost).to.have.been.calledWith('/api/:kind');
    expect(httpPost).to.have.been.calledWith('/api/:parent/:id/:kind');
    expect(httpGet).to.have.been.calledWith('/api/:kind');
    expect(httpGet).to.have.been.calledWith('/api/:parent/:id/:kind');
    expect(httpGet).to.have.been.calledWith('/api/:kind/:id');
    expect(httpDelete).to.have.been.calledWith('/api/:kind/:id');
    expect(httpPut).to.have.been.calledWith('/api/:kind/:id');
    expect(httpPatch).to.have.been.calledWith('/api/:kind/:id');
  });
  it('includes bodyParser middleware', () => {
    expect(getMiddlewares()).to.include.members([bodyParserMiddleware]);
  });
  it('includes default bodyParser middleware when a custom one is not given', () => {
    const options = { dependencies: { bodyParserMiddleware: undefined } };
    const middlewares = getMiddlewares(options);
    expect(middlewares).to.not.include.members([bodyParserMiddleware]);
    expect(middlewares.some(middleware => middleware.name === 'bodyParser')).to.be.true;
  });
  it('includes a middleware to set Access-Control-Allow-Origin header', () => {
    expect(getMiddlewares()).to.include.members([allowOriginMiddleware]);
  });
  it('includes the default middleware for setting Access-Control-Allow-Origin header when a custom one is not provided', () => {
    const options = { dependencies: { allowOriginMiddleware: undefined } };
    expect(getMiddlewares(options)).to.include.members([defaultAllowOriginMiddleware]);
  });
  it('includes a middleware that process pre-flighted requests', () => {
    const ctx = {
      _: 'ctx',
      set: stub(),
    };
    expect(getMiddlewares()).to.include.members([middlewareStubs.options]);
    expect(httpOptions).to.have.been.calledOnceWith('/my-generic-api/*');

    middlewareStubs.options(ctx);

    expect(ctx).to.have.property('status').that.is.equal(204);
    expect(ctx.set).to.have.been.calledOnceWithExactly({
      'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE',
      'Access-Control-Allow-Headers': 'Authorization, Content-Type, X-Requested-With',
    });
  });
  it('includes a middleware for authentication', async () => {
    const ctx = { _: 'ctx' };
    expect(getMiddlewares()).to.include.members([middlewareStubs.auth]);
    expect(httpPost).to.have.been.calledWith('/my-generic-api/auth');

    await middlewareStubs.auth(ctx);

    expect(ctx).to.have.property('status').that.is.equal(204);
    expect(authenticate).to.have.been.calledOnceWithExactly(ctx);
  });
  it('includes a middleware for creating a document', async () => {
    const ctx = { _: 'ctx' };
    expect(getMiddlewares()).to.include.members([middlewareStubs.create]);
    expect(httpPost).to.have.been.calledWith('/my-generic-api/:kind');

    await middlewareStubs.create(ctx, 'kind');

    expect(create).to.have.been.calledOnceWithExactly(ctx, 'kind', {
      authenticate, createCRUD: createCRUD, crud, realm: 'REALM',
    });
  });
  it('includes a middleware for creating a document associated with another document', async () => {
    const ctx = { _: 'ctx' };
    expect(getMiddlewares()).to.include.members([middlewareStubs.createUnderParent]);
    expect(httpPost).to.have.been.calledWith('/my-generic-api/:parent/:id/:kind');

    await middlewareStubs.createUnderParent(ctx, 'parent', 'id', 'kind');

    expect(createUnderParent).to.have.been.calledOnceWithExactly(ctx, 'parent', 'id', 'kind', {
      authenticate, createCRUD: createCRUD, crud, realm: 'REALM',
    });
  });
  it('includes a middleware for reading documents', async () => {
    const ctx = { _: 'ctx' };
    expect(getMiddlewares()).to.include.members([middlewareStubs.read]);
    expect(httpGet).to.have.been.calledWith('/my-generic-api/:kind');

    await middlewareStubs.read(ctx, 'kind');

    expect(read).to.have.been.calledOnceWithExactly(ctx, 'kind', {
      authenticate, createCRUD: createCRUD, crud, realm: 'REALM',
    });
  });
  it('includes a middleware for reading documents associated with another document', async () => {
    const ctx = { _: 'ctx' };
    expect(getMiddlewares()).to.include.members([middlewareStubs.readUnderParent]);
    expect(httpPost).to.have.been.calledWith('/my-generic-api/:parent/:id/:kind');

    await middlewareStubs.readUnderParent(ctx, 'parent', 'id', 'kind');

    expect(readUnderParent).to.have.been.calledOnceWithExactly(ctx, 'parent', 'id', 'kind', {
      authenticate, createCRUD: createCRUD, crud, realm: 'REALM',
    });
  });
  it('includes a middleware for reading a specific document', async () => {
    const ctx = { _: 'ctx' };
    expect(getMiddlewares()).to.include.members([middlewareStubs.readOne]);
    expect(httpGet).to.have.been.calledWith('/my-generic-api/:kind/:id');

    await middlewareStubs.readOne(ctx, 'kind', 'id');

    expect(readOne).to.have.been.calledOnceWithExactly(ctx, 'kind', 'id', {
      authenticate, createCRUD: createCRUD, crud, realm: 'REALM',
    });
  });
  it('includes a middleware for removing a specific document', async () => {
    const ctx = { _: 'ctx' };
    expect(getMiddlewares()).to.include.members([middlewareStubs.deleteOne]);
    expect(httpDelete).to.have.been.calledWith('/my-generic-api/:kind/:id');

    await middlewareStubs.deleteOne(ctx, 'kind', 'id');

    expect(deleteOne).to.have.been.calledOnceWithExactly(ctx, 'kind', 'id', {
      authenticate, createCRUD: createCRUD, crud, realm: 'REALM',
    });
  });
  it('includes a middleware for replacing a specific document', async () => {
    const ctx = { _: 'ctx' };
    expect(getMiddlewares()).to.include.members([middlewareStubs.replaceOne]);
    expect(httpPut).to.have.been.calledWith('/my-generic-api/:kind/:id');

    await middlewareStubs.replaceOne(ctx, 'kind', 'id');

    expect(replaceOne).to.have.been.calledOnceWithExactly(ctx, 'kind', 'id', {
      authenticate, createCRUD: createCRUD, crud, realm: 'REALM',
    });
  });
  it('includes a middleware for updating a specific document', async () => {
    const ctx = { _: 'ctx' };
    expect(getMiddlewares()).to.include.members([middlewareStubs.updateOne]);
    expect(httpPatch).to.have.been.calledWith('/my-generic-api/:kind/:id');

    await middlewareStubs.updateOne(ctx, 'kind', 'id');

    expect(updateOne).to.have.been.calledOnceWithExactly(ctx, 'kind', 'id', {
      authenticate, createCRUD: createCRUD, crud, realm: 'REALM',
    });
  });
});
