const auth = require('./auth');
const helpers = require('./helpers');

const read = async (ctx, kind, dependencies) => {
  const { authenticate = auth.authenticate, createCRUD, crud, realm } = dependencies;
  const authToken = await authenticate({ ctx, crud, realm });
  const directAllowed = helpers.isDirectAllowed(kind, authToken.perms, ['read', 'write']);

  ctx.assert(directAllowed, 403, 'Forbidden');
  crud[kind] = kind in crud ? crud[kind] : createCRUD(kind);
  ctx.body = await crud[kind].read();
  ctx.status = 200;
};

const readUnderParent = async (ctx, pKind, pId, kind, dependencies) => {
  const { authenticate = auth.authenticate, createCRUD, crud, realm } = dependencies;
  const authToken = await authenticate({ ctx, crud, realm });
  const reqPerms = ['read', 'write'];
  const directAllowed = helpers.isDirectAllowed(kind, authToken.perms, reqPerms);
  const allowed = directAllowed || helpers.containsAnyOf(authToken.perms[`${pKind}/${pId}/${kind}`], reqPerms);

  ctx.assert(allowed, 403, 'Forbidden');
  crud[kind] = kind in crud ? crud[kind] : createCRUD(kind);
  ctx.body = await crud[kind].read({ [`${pKind}_id`]: pId });
  ctx.status = 200;
};

const readOne = async (ctx, kind, id, dependencies) => {
  const { authenticate = auth.authenticate, createCRUD, crud, realm } = dependencies;
  const authToken = await authenticate({ ctx, crud, realm });
  const reqPerms = ['read', 'write'];
  const directAllowed = helpers.isDirectAllowed(kind, authToken.perms, reqPerms);
  const potentiallyAllowed = !directAllowed && helpers.isPotentiallyAllowed(kind, authToken.perms, reqPerms);
  let allowed, document;

  ctx.assert(directAllowed || potentiallyAllowed, 403, 'Forbidden');
  crud[kind] = kind in crud ? crud[kind] : createCRUD(kind);
  document = await crud[kind].read(id);
  ctx.assert(!!document, 404, 'Not Found');
  allowed = directAllowed || helpers.isAllowed(helpers.ind, document, authToken.perms, reqPerms);
  ctx.assert(allowed, 403, 'Forbidden');
  ctx.body = document;
  ctx.status = 200;
};

module.exports = { read, readOne, readUnderParent };
