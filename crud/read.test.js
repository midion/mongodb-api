const crypto = require('crypto');
const { afterEach, describe, it } = require('mocha');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const { read, readOne, readUnderParent } = require('./read');

chai.use(sinonChai);

describe('api/crud/read', () => {
  const { expect } = chai;
  const { stub, match: { any } } = sinon;
  const ctxAssertStub = stub();
  const ctxThrowStub = stub().throws();
  const ctxRequestGetStub = stub();

  ctxAssertStub.withArgs(true, any, any).returns();
  ctxAssertStub.withArgs(false, any, any).throws('AssertError');
  ctxRequestGetStub.withArgs('Authorization').returns(`Basic ${Buffer.from('john@example.com:secret').toString('base64')}`);

  afterEach(() => {
    ctxAssertStub.resetHistory();
    ctxThrowStub.resetHistory();
    ctxRequestGetStub.resetHistory();
  });

  describe('read', () => {
    it('reads documents when allowed', async () => {
      const authenticate = stub().resolves({
        perms: {
          'user': ['write'],
        },
      });
      const users = [
        { email: 'john@example', org_id: '54321' },
        { email: 'bart@example', org_id: '54321' },
      ];
      const crud = {
        user: {
          read: stub().resolves(users),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
      };
      await read(ctx, 'user', { authenticate, crud });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: undefined });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(true, 403, 'Forbidden');
      expect(crud.user.read).to.have.been.calledOnceWithExactly();
      expect(ctx.body).to.deep.equal(users);
      expect(ctx.status).to.equal(200);
    });
    it('throws when not allowed', async () => {
      const users = [
        { _id: '1', email: 'john@example', org_id: '54321' },
        { _id: '2', email: 'bart@example', org_id: '54321' },
      ];
      const crud = {
        user: {
          read: stub().resolves(users),
        },
        perm: {
          read: stub().resolves([{
            perms: {
              'org/54321/user': ['read'],
            },
          }]),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
        header: {},
        request: {
          get: ctxRequestGetStub,
        },
        response: {
          set: stub(),
        },
        throw: ctxThrowStub,
      };
      let error;

      try {
        await read(ctx, 'user', { crud, realm: 'Example' });
      } catch (err) {
        error = err;
      }


      expect(ctx.request.get).to.have.been.calledOnceWithExactly('Authorization');
      expect(ctx.response.set).to.have.been.calledOnceWith('X-Auth-Token');
      expect(ctx.throw).to.not.have.been.called;
      expect(ctx.header).to.not.have.property('WWW-Authenticate');

      expect(ctx.assert).to.have.been.calledOnceWithExactly(false, 403, 'Forbidden');
      expect(crud.user.read).to.have.been.calledOnceWithExactly({
        email: 'john@example.com',
        enabled: true,
        passcode: crypto.createHash('sha1').update('secret').digest('hex'),
      });
      expect(error).to.have.property('name').that.is.equal('AssertError');
    });
    it('reads documents when CRUD does not even exist yet', async () => {
      const authenticate = stub().resolves({
        perms: {
          'user': ['read'],
        },
      });
      const users = [
        { email: 'john@example', org_id: '123' },
        { email: 'bart@example', org_id: '234' },
      ];
      const createCRUD = stub().returns({ read: stub().resolves(users) });
      const crud = { _: 'crud' };
      const ctx = {
        assert: ctxAssertStub,
      };
      await read(ctx, 'user', { authenticate, createCRUD, crud });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: undefined });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(true, 403, 'Forbidden');
      expect(createCRUD).to.have.been.calledOnceWithExactly('user');
      expect(crud).to.have.property('user').that.is.a('object');
      expect(crud.user).to.have.property('read').that.is.a('function');
      expect(crud.user.read).to.have.been.calledOnceWithExactly();
      expect(ctx.body).to.deep.equal(users);
      expect(ctx.status).to.equal(200);
    });
  });

  describe('readUnderParent', () => {
    it('reads documents when direct allowed', async () => {
      const authenticate = stub().resolves({
        perms: {
          'user': ['read', 'write'],
          'org/1234/user': ['read'],
        },
      });
      const users = [
        { email: 'john@example', org_id: '54321' },
        { email: 'bart@example', org_id: '54321' },
      ];
      const crud = {
        user: {
          read: stub().resolves(users),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
      };
      await readUnderParent(ctx, 'org', '54321', 'user', { authenticate, crud });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: undefined });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(true, 403, 'Forbidden');
      expect(crud.user.read).to.have.been.calledOnceWithExactly({ org_id: '54321' });
      expect(ctx.body).to.deep.equal(users);
      expect(ctx.status).to.equal(200);
    });
    it('reads documents when allowed via parent', async () => {
      const users = [
        { _id: '1', email: 'john@example', group_id: '6789' },
        { _id: '2', email: 'bart@example', group_id: '6789' },
      ];
      const crud = {
        user: {
          read: stub().resolves(users),
        },
        perm: {
          read: stub().resolves([{
            perms: {
              'group/6789/user': ['read', 'write'],
            },
          }]),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
        header: {},
        throw: ctxThrowStub,
        request: {
          get: ctxRequestGetStub,
        },
        response: {
          set: stub(),
        },
      };

      await readUnderParent(ctx, 'group', '6789', 'user', { crud });

      expect(ctx.request.get).to.have.been.calledOnceWithExactly('Authorization');
      expect(ctx.response.set).to.have.been.calledOnceWith('X-Auth-Token');
      expect(ctx.throw).to.not.have.been.called;
      expect(ctx.header).to.not.have.property('WWW-Authenticate');

      expect(ctx.assert).to.have.been.calledOnceWithExactly(true, 403, 'Forbidden');
      expect(crud.user.read).to.have.been.calledWithExactly({ group_id: '6789' });
      expect(ctx.body).to.deep.equal(users);
      expect(ctx.status).to.equal(200);
    });
    it('throws when not allowed', async () => {
      const authenticate = stub().resolves({
        perms: {
          'org/1234/user': ['read'],
        },
      });
      const users = [
        { email: 'john@example', org_id: '54321' },
        { email: 'bart@example', org_id: '54321' },
      ];
      const crud = {
        user: {
          read: stub().resolves(users),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
      };
      let error;

      try {
        await readUnderParent(ctx, 'org', '54321', 'user', { authenticate, crud, realm: 'Example' });
      } catch (err) {
        error = err;
      }

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: 'Example' });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(false, 403, 'Forbidden');
      expect(crud.user.read).to.not.have.been.called;
      expect(error).to.have.property('name').that.is.equal('AssertError');
    });
    it('reads documents when CRUD does not even exist yet', async () => {
      const authenticate = stub().resolves({
        perms: {
          'group/1234/user': ['read'],
        },
      });
      const users = [
        { email: 'john@example', group_id: '1234' },
      ];
      const createCRUD = stub().returns({ read: stub().resolves(users) });
      const crud = { _: 'crud' };
      const ctx = {
        assert: ctxAssertStub,
      };
      await readUnderParent(ctx, 'group', '1234', 'user', { authenticate, createCRUD, crud });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: undefined });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(true, 403, 'Forbidden');
      expect(createCRUD).to.have.been.calledOnceWithExactly('user');
      expect(crud).to.have.property('user').that.is.a('object');
      expect(crud.user).to.have.property('read').that.is.a('function');
      expect(crud.user.read).to.have.been.calledOnceWithExactly({ group_id: '1234' });
      expect(ctx.body).to.deep.equal(users);
      expect(ctx.status).to.equal(200);
    });
  });

  describe('readOne', () => {
    it('reads document when allowed', async () => {
      const authenticate = stub().resolves({
        perms: {
          'user': ['write'],
        },
      });
      const user = { email: 'john@example', org_id: '54321' };
      const crud = {
        user: {
          read: stub().resolves(user),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
      };
      await readOne(ctx, 'user', '123', { authenticate, crud });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: undefined });
      expect(ctx.assert).to.have.been.calledThrice;
      expect(ctx.assert).to.have.been.calledWithExactly(true, 403, 'Forbidden');
      expect(ctx.assert).to.have.been.calledWithExactly(true, 404, 'Not Found');
      expect(ctx.assert).to.not.have.been.calledWith(false);
      expect(crud.user.read).to.have.been.calledOnceWithExactly('123');
      expect(ctx.body).to.deep.equal(user);
      expect(ctx.status).to.equal(200);
    });
    it('reads document when allowed and CRUD does not exist', async () => {
      const authenticate = stub().resolves({
        perms: {
          'user': ['write'],
        },
      });
      const user = { email: 'john@example', org_id: '54321' };
      const createCRUD = stub().returns({ read: stub().resolves(user) });
      const crud = { _: 'crud' };
      const ctx = {
        assert: ctxAssertStub,
      };
      await readOne(ctx, 'user', '123', { authenticate, createCRUD, crud });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: undefined });
      expect(createCRUD).to.have.been.calledOnceWithExactly('user');
      expect(ctx.assert).to.have.been.calledThrice;
      expect(ctx.assert).to.have.been.calledWithExactly(true, 403, 'Forbidden');
      expect(ctx.assert).to.have.been.calledWithExactly(true, 404, 'Not Found');
      expect(ctx.assert).to.not.have.been.calledWith(false);
      expect(crud.user.read).to.have.been.calledOnceWithExactly('123');
      expect(ctx.body).to.deep.equal(user);
      expect(ctx.status).to.equal(200);
    });
    it('throws when not allowed', async () => {
      const user = { _id: '1', email: 'john@example', org_id: '54321' };
      const users = [
        { _id: '1', email: 'john@example', org_id: '54321' },
        { _id: '2', email: 'bart@example', org_id: '54321' },
      ];
      const crud = {
        user: {
          read: ((stub) => (
            stub.withArgs('1').resolves(user),
            stub.resolves(users),
            stub
          ))(stub()),
        },
        perm: {
          read: stub().resolves([{
            perms: {
              'org/3421/user': ['read'],
            },
          }]),
        },
      };
      const ctx = {
        assert: ctxAssertStub,
        header: {},
        throw: ctxThrowStub,
        request: {
          get: ctxRequestGetStub
        },
        response: {
          set: stub(),
        },
      };
      let error;

      try {
        await readOne(ctx, 'user', '1', { crud, realm: 'Example' });
      } catch (err) {
        error = err;
      }

      expect(ctx.request.get).to.have.been.calledOnceWithExactly('Authorization');
      expect(ctx.response.set).to.have.been.calledOnceWith('X-Auth-Token');
      expect(ctx.throw).to.not.have.been.called;
      expect(ctx.header).to.not.have.property('WWW-Authenticate');
      expect(ctx.assert).to.have.been.calledThrice;
      expect(ctx.assert).to.have.been.calledWithExactly(true, 403, 'Forbidden');
      expect(ctx.assert).to.have.been.calledWithExactly(true, 404, 'Not Found');
      expect(ctx.assert).to.have.been.calledWithExactly(false, 403, 'Forbidden');
      expect(crud.user.read).to.have.been.calledWithExactly({
        email: 'john@example.com',
        enabled: true,
        passcode: crypto.createHash('sha1').update('secret').digest('hex'),
      });
      expect(crud.user.read).to.have.been.calledWithExactly('1');
      expect(error).to.have.property('name').that.is.equal('AssertError');
    });
    it('reads documents when CRUD does not even exist yet', async () => {
      const authenticate = stub().resolves({
        perms: {
          'user': ['read'],
        },
      });
      const users = [
        { email: 'john@example', org_id: '123' },
        { email: 'bart@example', org_id: '234' },
      ];
      const createCRUD = stub().returns({ read: stub().resolves(users) });
      const crud = { _: 'crud' };
      const ctx = {
        assert: ctxAssertStub,
      };
      await read(ctx, 'user', { authenticate, createCRUD, crud });

      expect(authenticate).to.have.been.calledOnceWithExactly({ ctx, crud, realm: undefined });
      expect(ctx.assert).to.have.been.calledOnceWithExactly(true, 403, 'Forbidden');
      expect(createCRUD).to.have.been.calledOnceWithExactly('user');
      expect(crud).to.have.property('user').that.is.a('object');
      expect(crud.user).to.have.property('read').that.is.a('function');
      expect(crud.user.read).to.have.been.calledOnceWithExactly();
      expect(ctx.body).to.deep.equal(users);
      expect(ctx.status).to.equal(200);
    });
  });
});
