const auth = require('./auth');
const helpers = require('./helpers');

const replaceOne = async (ctx, kind, id, dependencies) => {
  const { authenticate = auth.authenticate, createCRUD, crud, realm } = dependencies;
  const authToken = await authenticate({ ctx, crud, realm });
  const reqPerms = ['update', 'write'];
  const directAllowed = helpers.isDirectAllowed(kind, authToken.perms, reqPerms);
  const potentiallyAllowed = !directAllowed && helpers.isPotentiallyAllowed(kind, authToken.perms, reqPerms);
  let allowed, document;

  ctx.assert(directAllowed || potentiallyAllowed, 403, 'Forbidden');
  crud[kind] = kind in crud ? crud[kind] : createCRUD(kind);
  document = await crud[kind].read(id);
  ctx.assert(!!document, 404, 'Not Found');
  allowed = directAllowed || helpers.isAllowed(kind, [document, ctx.request.body], authToken.perms, reqPerms);
  ctx.assert(allowed, 403, 'Forbidden');
  ctx.request.body._id = document._id;
  ctx.body = await crud[kind].update(ctx.request.body);
  ctx.status = 204;
};

const updateOne = async (ctx, kind, id, dependencies) => {
  const { authenticate = auth.authenticate, createCRUD, crud, realm } = dependencies;
  const authToken = await authenticate({ ctx, crud, realm });
  const reqPerms = ['update', 'write'];
  const directAllowed = helpers.isDirectAllowed(kind, authToken.perms, reqPerms);
  const potentiallyAllowed = !directAllowed && helpers.isPotentiallyAllowed(kind, authToken.perms, reqPerms);
  let allowed, document;

  ctx.assert(directAllowed || potentiallyAllowed, 403, 'Forbidden');
  crud[kind] = kind in crud ? crud[kind] : createCRUD(kind);
  document = await crud[kind].read(id);
  ctx.assert(!!document, 404, 'Not Found');
  allowed = directAllowed || helpers.isAllowed(kind, [document, ctx.request.body], authToken.perms, reqPerms);
  ctx.assert(allowed, 403, 'Forbidden');
  ctx.request.body._id = document._id;
  ctx.body = await crud[kind].update(id, ctx.request.body);
  ctx.status = 204;
};

module.exports = { replaceOne, updateOne };
