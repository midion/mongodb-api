const Koa = require('koa');
const route = require('koa-route');
const { Console } = require('console');
const { version } = require('./package.json');
const { middlewares } = require('./crud');

const console = new Console(process.stdout);

const PORT = process.env.PORT || 8010;
const DBNAME = process.env.DBNAME || 'api';
const REALM = process.env.REALM || 'MongoDB REST API';
const BASE_URI = process.env.BASE_URI || '/api';

const app = new Koa();

app.silent = true;
app.use(route.get(BASE_URI, ctx => {
  ctx.body = {
    description: 'API version',
    version,
  };
}));
app.middleware.push(...middlewares({
  baseURI: BASE_URI,
  database: DBNAME,
  realm: REALM,
}));

const server = app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});

module.exports = { server };
