const { after, before, describe, it } = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');
const sinon = require('sinon');
const { version } = require('./package.json');
const R85 = require('r85');
const { server } = require('./');

chai.use(chaiHttp);

describe('api', () => {
  const { expect } = chai;
  const { stub } = sinon;
  const r85 = new R85(process.env.R85KEY);
  const api = chai.request(server).keepOpen();
  const shared = {};
  const authTokenBearer = r85.encode(JSON.stringify({
    expiry: 1100000,
    perms: {
      misc: ['write'],
    },
  }));
  let nowStub;

  before(() => {
    nowStub = stub(Date, 'now').returns(1000000);
  });

  after(done => {
    nowStub.restore();
    api.close(done);
  });

  it('responds with API version', async () => {
    const res = await api.get('/api');

    expect(res).to.have.status(200);
    expect(res).to.be.json;
    expect(res.body).to.deep.equal({ description: 'API version', version });
  });

  it('creates a document', async () => {
    const res = await api.post('/api/misc').set('Authorization', `Bearer ${authTokenBearer}`).send({
      propA: 'Value of prop A',
      propB: 'Value of prop B',
      propC: 'Value of prop C',
    });

    expect(res).to.have.status(201);
    expect(res).to.be.json;
    expect(res.body).to.be.a('string');

    shared.id = res.body;
  });

  it('retrieves the document', async () => {
    const res = await api.get(`/api/misc/${shared.id}`).set('Authorization', `Bearer ${authTokenBearer}`);

    expect(res).to.have.status(200);
    expect(res).to.be.json;
    expect(res.body).to.deep.equal({
      _id: shared.id,
      propA: 'Value of prop A',
      propB: 'Value of prop B',
      propC: 'Value of prop C',
    });
  });

  it('retrieves all documents', async () => {
    const res = await api.get('/api/misc').set('Authorization', `Bearer ${authTokenBearer}`);

    expect(res).to.have.status(200);
    expect(res).to.be.json;
    expect(res.body).to.be.an('array').that.deep.includes({
      _id: shared.id,
      propA: 'Value of prop A',
      propB: 'Value of prop B',
      propC: 'Value of prop C',
    });
  });

  it('replaces the document', async () => {
    const res = await api.put(`/api/misc/${shared.id}`).set('Authorization', `Bearer ${authTokenBearer}`).send({
      propA: 'Value of prop A',
      propC: 'Value of prop C',
    });

    expect(res).to.have.status(204);
    expect(res.body).to.be.empty;
  });

  it('retrieves the replaced document', async () => {
    const res = await api.get(`/api/misc/${shared.id}`).set('Authorization', `Bearer ${authTokenBearer}`);

    expect(res).to.have.status(200);
    expect(res).to.be.json;
    expect(res.body).to.deep.equal({
      _id: shared.id,
      propA: 'Value of prop A',
      propC: 'Value of prop C',
    });
  });

  it('updates the document', async () => {
    const res = await api.patch(`/api/misc/${shared.id}`).set('Authorization', `Bearer ${authTokenBearer}`).send({
      propA: 'Value of prop A modified',
      propB: 'Value of prop B',
    });

    expect(res).to.have.status(204);
    expect(res.body).to.be.empty;
  });

  it('retrieves the updated document', async () => {
    const res = await api.get(`/api/misc/${shared.id}`).set('Authorization', `Bearer ${authTokenBearer}`);

    expect(res).to.have.status(200);
    expect(res).to.be.json;
    expect(res.body).to.deep.equal({
      _id: shared.id,
      propA: 'Value of prop A modified',
      propB: 'Value of prop B',
      propC: 'Value of prop C',
    });
  });

  it('deletes the document', async () => {
    const res = await api.del(`/api/misc/${shared.id}`).set('Authorization', `Bearer ${authTokenBearer}`);

    expect(res).to.have.status(204);
    expect(res.body).to.be.empty;
  });

  it('deletes the document again', async () => {
    const res = await api.del(`/api/misc/${shared.id}`).set('Authorization', `Bearer ${authTokenBearer}`);

    expect(res).to.have.status(404);
    expect(res.body).to.be.empty;
  });

  it('retrieves the deleted document', async () => {
    const res = await api.get(`/api/misc/${shared.id}`).set('Authorization', `Bearer ${authTokenBearer}`);

    expect(res).to.have.status(404);
    expect(res.body).to.be.empty;
  });

  it('replaces the deleted document', async () => {
    const res = await api.put(`/api/misc/${shared.id}`).set('Authorization', `Bearer ${authTokenBearer}`).send({
      propA: 'Value of prop A',
    });

    expect(res).to.have.status(404);
    expect(res.body).to.be.empty;
  });

  it('updates the deleted document', async () => {
    const res = await api.patch(`/api/misc/${shared.id}`).set('Authorization', `Bearer ${authTokenBearer}`).send({
      propA: 'Value of prop A modified',
    });

    expect(res).to.have.status(404);
    expect(res.body).to.be.empty;
  });

  /*describe('exceptions', () => {
    const sandbox = sinon.createSandbox();

    before(() => {
      sandbox.replace(crud.misc, 'create', sinon.fake.throws());
      sandbox.replace(crud.misc, 'read', sinon.fake.throws());
      sandbox.replace(crud.misc, 'update', sinon.fake.throws());
      sandbox.replace(crud.misc, 'delete', sinon.fake.throws());
    });

    after(() => {
      sandbox.restore();
    });

    it('returns status 500 when create fails', async () => {
      const res = await api.post('/api/misc').send({});

      expect(res).to.have.status(500);
      expect(res).to.be.text;
      expect(res.text).to.equals('Internal Server Error');
    });

    it('returns status 500 when read one fails', async () => {
      const res = await api.get('/api/misc/12345');

      expect(res).to.have.status(500);
      expect(res).to.be.text;
      expect(res.text).to.equals('Internal Server Error');
    });

    it('returns status 500 when read many fails', async () => {
      const res = await api.get('/api/misc');

      expect(res).to.have.status(500);
      expect(res).to.be.text;
      expect(res.text).to.equals('Internal Server Error');
    });

    it('returns status 500 when replace fails', async () => {
      const res = await api.put('/api/misc/12345').send({});

      expect(res).to.have.status(500);
      expect(res).to.be.text;
      expect(res.text).to.equals('Internal Server Error');
    });

    it('returns status 500 when update fails', async () => {
      const res = await api.patch('/api/misc/12345').send({});

      expect(res).to.have.status(500);
      expect(res).to.be.text;
      expect(res.text).to.equals('Internal Server Error');
    });

    it('returns status 500 when delete fails', async () => {
      const res = await api.del('/api/misc/12345');

      expect(res).to.have.status(500);
      expect(res).to.be.text;
      expect(res.text).to.equals('Internal Server Error');
    });
  });*/
});
